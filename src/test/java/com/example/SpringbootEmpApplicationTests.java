package com.example;

import com.example.entity.Emp;
import com.example.service.EmpService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class SpringbootEmpApplicationTests {

    @Autowired
    private EmpService empService;
    @Test
    void contextLoads() {
        Emp emp = empService.getById(1);
        System.out.println(emp);
    }

}
