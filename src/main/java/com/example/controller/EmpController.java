package com.example.controller;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.example.common.R;
import com.example.entity.Emp;
import com.example.service.EmpService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.Cookie;
import java.util.List;


@Slf4j
@RestController
@RequestMapping("/emp")
public class EmpController {

    @Autowired
    private EmpService empService;

    @GetMapping("/list")
    public R<List<Emp>> getEmp(Integer id){
        log.info("查询数据中");
        System.err.println("董梦婷是个小胖墩....");
        List<Emp> emps = empService.getAll();
        return R.success(emps);
    }

    @GetMapping("/list/{id}")
    public R<Emp> getEmpById(@PathVariable int id){
        log.info("查询数据中");
        Emp emp = empService.getById(id);
        return R.success(emp);
    }

    @DeleteMapping("/delete/{id}")
    public R<String> deleteEmp(@PathVariable int id){
        log.info("id={}",id);
        empService.removeById(id);

        //TODO R.success("success") 这样写是不恰当的
        return R.success("success");
    }

    @PostMapping("/add")
    public R<String> saveAdd(@RequestBody Emp emp){
        log.info("添加操作");
        System.out.println(emp);
        empService.save(emp);

        return R.success("success");
    }

    @PutMapping("/list")
    public R<String> updateById(@RequestBody Emp emp){
        log.info("更新操作");
        boolean b = empService.updateById(emp);
        return  R.success("");
    }


}
