package com.example.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.example.entity.Emp;

import java.util.List;

public interface EmpService extends IService<Emp> {
    public List<Emp> getAll();
}
