package com.example.entity;

import lombok.Data;

import java.io.Serializable;

@Data
public class Emp implements Serializable {
    private Integer id;
    private String name;
//    private String image;
    private int sex;
    private String job;
    private String entrydate;
    private String updatetime;
}
